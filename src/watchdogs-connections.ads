with Ada.Finalization;
with Ada.Task_Identification;

with Watchdogs.Alarm_Handlers;

private with Watchdogs.Watchers;

use Ada;

package Watchdogs.Connections is
   use type Task_Identification.Task_Id;

   --
   -- Type representing the watcher, that is, the object that wakes up
   -- and checks if its tasks are still alive.
   --
   type Watcher_Type is private;

   --
   -- Create a new watcher type specifying an alarm handler and a sampling
   -- time, that is, the time interval between successive wake ups.
   --
   function Create (Alarm_Handler : Alarm_Handlers.Alarm_Handler_Access;
                    Sampling      : Duration := 1.0)
                    return Watcher_Type;


   --
   -- A watchdog connection allows a task to communicate with the
   -- watcher
   --
   type Watchdog_Connection (<>) is limited private;

   --
   -- Open a connection with the watcher.  The task needs to introduce itself
   -- with a name or a Task_ID, possibly boty.  Those values will be passed
   -- to the Alarm_Handler if the task becomes unresponsive.
   --
   function Open (Watchdog  : Watcher_Type;
                  Task_Name : String := "";
                  ID        : Task_Identification.Task_Id := Task_Identification.Null_Task_Id)
                  return Watchdog_Connection
     with
       Pre => (Task_Name /= "" or ID /= Task_Identification.Null_Task_Id);

   --
   -- Let the watcher know that we are still alive.  If this function is
   -- called in different points of the task it is possible to distinguish
   -- different calls via the Checkpoint parameter.  The reason for having
   -- Objective is just to know what is the latest instance of I_Am_Alive
   -- called before the task crash.  Its value will be given to the
   -- alarm handler.
   --
   procedure I_Am_Alive (Connection : in out Watchdog_Connection;
                         Checkpoint : Checkpoint_Type := 0);

private
   type Watcher_Type is access Watchers.Watchdog_Core;

   type Watchdog_Connection is
     new Ada.Finalization.Limited_Controlled with
      record
         Watchdog   : Watcher_Type;
         Connection : Watchers.Connection_ID;
      end record;

   overriding procedure Finalize (Item : in out Watchdog_Connection);
end Watchdogs.Connections;
