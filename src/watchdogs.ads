

--
-- This hierarchy of packages was born after reading an article about
-- how to write a thread watchdog in C.  I said "Ehi, that would be
-- a nice Ada project!"
--
-- So, here it is.
--
-- The objective of this library is to verify if a task is still
-- alive or not and if not raise an alarm.  This is done with the
-- help of three "objects"
--
-- * The watcher (Watcher_Type in Watchdogs.Connections)
-- * The connection to the watcher  (Watchdog_Connection in Watchdogs.Connections)
-- * An alarm handler, a descendent of Alarm_Handler_Interface in
--   Watchdogs.Alarm_Handlers
--
-- The working is really simple
--
-- 1. The watcher wakes up at regular times and check if the task that is
--    monitoring recently claimed to be still alive.  For any task that
--    did not declare being alive, it calls the alarm handler that will
--    do something (write a message to log or standard error, kill the
--    program, restart the task, bake a cake, anything...)
--
-- 2. Every task that needs to be monitored open a connection with the
--    watcher and regularly declare its being alive by calling the
--    procedure I_Am_Alive
--
-- Every watcher is an object, so that the code can have more than one
-- watcher.  Every watcher has its sampling time and alarm handler.
--
package Watchdogs is
   type Checkpoint_Type is mod 2 ** 16;

   No_Checkpoint : constant Checkpoint_Type := Checkpoint_Type'Last;
end Watchdogs;
