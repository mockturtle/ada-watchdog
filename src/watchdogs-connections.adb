pragma Ada_2012;

package body Watchdogs.Connections is

   ------------
   -- Create --
   ------------

   function Create
     (Alarm_Handler : Alarm_Handlers.Alarm_Handler_Access;
      Sampling      : Duration := 1.0)
      return Watcher_Type
   is

   begin
      return new Watchers.Watchdog_Core'(Watchers.Create (Alarm_Handler, Sampling));
   end Create;

   ----------------
   -- I_Am_Alive --
   ----------------

   procedure I_Am_Alive
     (Connection : in out Watchdog_Connection;
      Checkpoint :        Checkpoint_Type := 0)
   is
   begin
      Watchers.Mark_Alive (Core       => Connection.Watchdog.all,
                           Connection => Connection.Connection,
                           Checkpoint => Checkpoint);
   end I_Am_Alive;

   ----------
   -- Open --
   ----------

   function Open
     (Watchdog  : Watcher_Type;
      Task_Name : String := "";
      ID        : Task_Identification.Task_Id := Task_Identification.Null_Task_Id)
      return Watchdog_Connection
   is
      Connection : Watchers.Connection_ID;
   begin
      Watchers.Get_Connection (Core       => Watchdog.all,
                               Task_Name  => Task_Name,
                               ID         => id,
                               Connection => connection);

      return Watchdog_Connection'(Finalization.Limited_Controlled with
                                    Watchdog   => Watchdog,
                                  Connection => Connection);
   end Open;


   --------------
   -- Finalize --
   --------------

   overriding procedure Finalize (Item : in out Watchdog_Connection)
   is
   begin
      Watchers.Close (Core       => Item.Watchdog.all,
                      Connection => Item.Connection);
   end Finalize;

end Watchdogs.Connections;
